#!/bin/bash

if [[ ! -f /usr/bin/memcached ]]; then
  yum -y install memcached
  /etc/init.d/memcached start
  chkconfig memcached on
fi
